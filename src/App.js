import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from './component/Home'
import displayMovie from "./component/displayMovie"
import displayTv_Shows from "./component/displayTv_Shows"
import tv_ShowsDetail from "./component/tv_ShowsDetail"
import movieDetail from "./component/movieDetail"
import "./App.css"

class App extends Component {
  render() {
    return (
      <BrowserRouter className='main_div'>
        <Route path='/' exact component={Home} />
        <Route path='/Movies' component={displayMovie} />
        <Route path='/Tv_Show' component={displayTv_Shows} />
        <Route path='/movieDetail/:id' component={movieDetail} />
        <Route path='/tv_ShowsDetail/:id' component={tv_ShowsDetail} />
      </BrowserRouter>
    );
  }
}
export default App;