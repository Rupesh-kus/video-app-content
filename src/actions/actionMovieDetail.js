import config from '../config/index'
export const MOVIES_DETAILS = "MOVIES_DETAILS"
export const MOVIES_TRAILER = "MOVIES_TRAILER"

export const actionMovieDetail = (id) => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/movie/${id}?api_key=${config.api_key}`
  )
    .then((res) => res.json())
    .then((moviesDetail) =>
      // console.log(moviesDetail, 'actionData')
      dispatch({
        type: MOVIES_DETAILS,
        payload: moviesDetail
      })
    )
}

export const movieTrailer = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((trailer) =>
      // console.log(trailer, 'data')
      dispatch({
        type: MOVIES_TRAILER,
        payload: trailer
      })
    )
}