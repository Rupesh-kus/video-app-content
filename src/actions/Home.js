import config from '../config/index'
export const HOME_MOVIES = "HOME_MOVIES"
export const Home_TVSHOWS = "Home_TVSHOWS"

export const homeMovies = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((homeMoviesData) =>
      // console.log(homeMoviesData, 'data')
      dispatch({
        type: HOME_MOVIES,
        payload: homeMoviesData
      })
    )
}

export const homeTvShow = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/tv/popular?api_key=${config.api_key}&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((homeTvData) =>
      // console.log(homeMoviesData, 'data')
      dispatch({
        type: Home_TVSHOWS,
        payload: homeTvData
      })
    )
}