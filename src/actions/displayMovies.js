import config from '../config/index'
export const DISPLAY_MOVIES = "DISPLAY_MOVIES"

export const displayMovies = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((MoviesData) =>
      // console.log(MoviesData, 'data')
      dispatch({
        type: DISPLAY_MOVIES,
        payload: MoviesData
      })
    )
}


export const searchMovies = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((MoviesData) =>
      // console.log(MoviesData, 'data')
      dispatch({
        type: DISPLAY_MOVIES,
        payload: MoviesData
      })
    )
}