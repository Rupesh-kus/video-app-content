// import config from '../config/index'
export const display_TVSHOWS = "display_TVSHOWS"

export const actionDisplayTv = () => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/tv/popular?api_key=1b035df2983fdd96a5fad1b7c802f49f&language=en-US&page=1`
  )
    .then((res) => res.json())
    .then((tvData) =>
      // console.log(tvData, 'data')
      dispatch({
        type: display_TVSHOWS,
        payload: tvData
      })
    )
}