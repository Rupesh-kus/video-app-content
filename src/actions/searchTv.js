import config from '../config/index'
export const SEARCH_TVSHOWS = "SEARCH_TVSHOWS"

export const searchTv = (searchQuery) => dispatch => {

  fetch
    (`https://api.themoviedb.org/3/search/tv?api_key=${config.api_key}&page=1&query=${searchQuery}&include_adult=false`)
    .then((res) => res.json())
    .then((searchResult) =>

      dispatch({
        type: SEARCH_TVSHOWS,
        payload: searchResult
      })
    )
}