import config from '../config/index'
export const TVSHOWS_DETAILS = "TVSHOWS_DETAILS"
export const TV_TRAILOR = "TV_TRAILOR"

export const tvShows_Details = (id) => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/tv/${id}?api_key=${config.api_key}`
  )
    .then((res) => res.json())
    .then((tv_Detail) =>
      // console.log(tv_Detail, 'hello')
      dispatch({
        type: TVSHOWS_DETAILS,
        payload: tv_Detail
      })
    )
}

export const trailer = (id) => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/tv/${id}/videos?api_key=${config.api_key}`
  )
    .then((res) => res.json())
    .then((trailor) =>
      // console.log(tv_Detail, 'hello')
      dispatch({
        type: TV_TRAILOR,
        payload: trailor
      })
    )
}