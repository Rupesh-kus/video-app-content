import config from '../config/index'
export const SEARCH_MOVIES = "SEARCH_MOVIES"

export const searchMovies = (searchQuery) => dispatch => {

  fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${config.api_key}&language=en-US&query=${searchQuery}&page=1&include_adult=false`
  )
    .then((res) => res.json())
    .then((searchResult) =>
      // console.log(searchResult, 'hello')
      dispatch({
        type: SEARCH_MOVIES,
        payload: searchResult
      })
    )
}