import React, { Component } from 'react'
import { connect } from "react-redux"
import { homeMovies, homeTvShow } from "../actions/Home"
import { Link } from "react-router-dom"
import config from '../config'

class Home extends Component {
  componentDidMount() {
    this.props.homeMovies();
    this.props.homeTvShow();
  }
  render() {
    // console.log(this.props.homeMoviesData, "movieData")
    // console.log(this.props.homeTvData, "Tv")
    return (
      <div>
        <nav aria-label="breadcrumb">
          <h4 style={{ textAlign: "center" }}> Video-Content-App</h4>
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/Movies">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Movies</span>
              </Link>
            </li>
            <li>
              <Link to="/Tv_Show">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Tv_Show</span>
              </Link>
            </li>
          </ol>
        </nav>
        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
          <div className="carousel-inner">
            {this.props.homeMoviesData.slice(1, 5).map((movieData, index) => (
              <div className={`carousel-item ${index === 0 ? "active" : ""}`}>
                <img className="imgStyle" src={`${config.img_baseURL}${movieData.poster_path}`} alt="..."
                />
              </div>
            ))}
          </div>
          <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>

        <br />
        <br />
        <h2 className="heading1"> Up-comming Movies</h2>
        <div className='row'>
          {this.props.homeMoviesData.slice(1, 13).map((movieData) => (
            <div className='col-md-3'>
              <div> {movieData.poster_path ?
                <img
                  className='img'
                  src={`${config.img_baseURL}${movieData.poster_path}`}
                  class='card-img-top'
                  alt='not found'
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${movieData.name}`}
                />
              }
                <p className='movietitle'>{movieData.title}</p>
                <Link
                  to={`/MovieDetail/${movieData.id}`}
                  class='btn btn-success'
                >
                  Veiw more
                </Link>
              </div>
            </div>
          ))}
        </div>
        <br />
        <h2 className="heading1"> Popular Tv-shows</h2>
        <div className="row">
          {this.props.homeTvData.slice(1, 13).map((TvshowData) => (
            <div className='col-md-3'>
              {TvshowData.poster_path ?
                <img
                  src={`${config.img_baseURL}${TvshowData.poster_path}`}
                  className='card-img-top'
                  alt={`Poster-${TvshowData.name}`}
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${TvshowData.name}`}
                />
              }
              <h2 className='movietitle'>{TvshowData.name}</h2>
              <Link
                to={`/Tvshowsdetail/${TvshowData.id}`}
                className='btn btn-success'
              >
                Veiw more
            </Link>
            </div>
          ))}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  homeMoviesData: state.homeVideos.homeMovies,
  homeTvData: state.homeVideos.homeTv
});


export default connect(mapStateToProps, { homeMovies, homeTvShow })(Home);