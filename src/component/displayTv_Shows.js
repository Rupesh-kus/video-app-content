import React, { Component } from 'react'
import { connect } from "react-redux"
import { actionDisplayTv } from "../actions/actionDisplayTv"
import { Link } from "react-router-dom"
import config from '../config'
import { searchTv } from '../actions/searchTv'
class displayTv_Shows extends Component {
  componentDidMount() {
    this.props.actionDisplayTv();
  }

  handleSearch = (e) => {
    this.props.searchTv(e.target.value)
  }

  render() {
    const { displayTvData, searchResult, searchQuery } = this.props;
    const data = searchResult.length > 0 ? searchResult : displayTvData;
    //console.log(data, "ok")
    //console.log(this.props.displayTvData, "123")
    return (
      <>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/TVshows">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>TVshows</span>
              </Link>
            </li>
          </ol>
        </nav>
        <input
          type='text'
          class='searchbutton'
          onChange={this.handleSearch}
          placeholder='Search tv_shows'
        />
        <div className='row'>
          {data.map((showData) => (
            <div className='col-md-3'>
              <div className='card'>
                <div className='card-body'>
                  {showData.poster_path ?
                    <img
                      src={`${config.img_baseURL}${showData.poster_path}`}
                      className='card-img-top'
                      alt={`Poster-${showData.original_name}`}
                    />
                    :
                    <img
                      src={require('./poster.png')}
                      className='card-img-top'
                      alt={`not-found-poster-${showData.original_name}`}
                    />
                  }
                </div>
                <div className='card-body'>
                  <h2 className='movietitle'>{showData.original_name}</h2>
                  <Link
                    to={`/tv_ShowsDetail/${showData.id}`}
                    className='btn btn-success'
                  >
                    Veiw more
            </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    )
  }
}

const mapStateToProps = (state) => ({
  displayTvData: state.Tv_show.display_Tv,
  searchResult: state.Tv_show.searchResult
});

export default connect(mapStateToProps, { actionDisplayTv, searchTv })(displayTv_Shows);