import React, { Component } from 'react';
import { connect } from "react-redux"
import { displayMovies } from "../actions/displayMovies"
import { Link } from "react-router-dom"
import config from '../config'
import { searchMovies } from "../actions/searchMovies"

class displayMovie extends Component {
  componentDidMount() {
    this.props.displayMovies();
  }

  handleSearch = (e) => {
    this.props.searchMovies(e.target.value)
  }

  render() {
    const { displayMovieData, searchResult, searchQuery } = this.props;
    const data = searchResult.length > 0 ? searchResult : displayMovieData;
    // console.log(data, "ok")

    return (
      <>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/Movies">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Movies</span>
              </Link>
            </li>
          </ol>
        </nav>
        <input
          type='text'
          class='searchbutton'
          onChange={this.handleSearch}
          placeholder='Search movies'
        />
        <div className='row'>
          {data.map((movieData) => (
            <div className='col-md-3'>
              <div className='card'>
                <div className='card-body'>
                  {movieData.poster_path ?
                    <img
                      className='img'
                      src={`${config.img_baseURL}${movieData.poster_path}`}
                      class='card-img-top'
                      alt='not found'
                    />
                    :
                    <img
                      src={require('./poster.png')}
                      className='card-img-top'
                      alt={`not-found-poster-${movieData.name}`}
                    />
                  }
                  <div class='card-body'>
                    <p className='movietitle'>{movieData.title}</p>
                    <Link
                      to={`/movieDetail/${movieData.id}`}
                      class='btn btn-success'
                    >
                      Veiw more
                </Link>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    )
  }
}

const mapStateToProps = (state) => ({
  displayMovieData: state.movie.display_M,
  searchResult: state.movie.searchResult
});

export default connect(mapStateToProps, { displayMovies, searchMovies })(displayMovie);