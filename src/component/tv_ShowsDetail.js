import React, { Component } from 'react'
import { connect } from "react-redux"
import { tvShows_Details, trailer } from "../actions/tvShows_Details"
import { Link } from "react-router-dom"
import config from '../config/index'
class tv_ShowsDetail extends Component {
  componentDidMount() {
    this.props.tvShows_Details(this.props.match.params.id);
    this.props.trailer(this.props.match.params.id);
  }
  render() {
    // console.log(this.props.detail, 'tvDetails111')
    //console.log(this.props.trailor, "123")
    return (
      <div>
        <ol className="breadcrumb">
          <li key="Homelink" className="breadcrumb-item">
            <Link to="/">
              <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>Home</span>
            </Link>
          </li>
          <li>
            <Link to="/TVshows">
              <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>TVshows</span>
            </Link>
          </li>
          <li>
            <Link to="/Moviedetails">
              <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>{this.props.detail.name}</span>
            </Link>
          </li>
        </ol>
        <div className="row row1" >
          <div className="col-md-6">
            <div className='card-body'>
              {this.props.detail.poster_path ?
                <img
                  src={`${config.img_baseURL}${this.props.detail.poster_path}`}
                  className='card-img-top'
                  alt={`Poster-${this.props.detail.name}`}
                  style={{ height: 450, width: 350 }}
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${this.props.detail.name}`}
                  style={{ height: 450, width: 350 }} />
              }
            </div>
          </div>
          <div className="col-md-6">
            <div className='card-body'>
              <h2 className='movietitle'>{this.props.detail.name}</h2>
              <h4 className='ratingsign'>
                Rating:
              {this.props.detail.vote_average}/10
            </h4>
              <h4 className='popularity'>
                Popularity:{this.props.detail.popularity}
              </h4>
              <h4 className='number_of_episodes'>
                number_of_episodes:{this.props.detail.number_of_episodes}
              </h4>
              <h4 className='number_of_seasons'>
                number_of_seasons:{this.props.detail.number_of_seasons}
              </h4>
              <h5 className='overview'>
                overview:{this.props.detail.overview}
              </h5>
            </div>
          </div>
          <div>
            <h1 style={{ color: "red", textAlign: "center" }}>Trailer</h1>
            {this.props.trailor.map((data) => (
              <iframe
                height='400px'
                width='550px'
                src={`https://www.youtube.com/embed/${data.key}`}
              />
            ))}
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  detail: state.details.detail_Tv,
  trailor: state.details.trailer
})

export default connect(mapStateToProps, { tvShows_Details, trailer })(tv_ShowsDetail);