import { MOVIES_DETAILS } from "../actions/actionMovieDetail"

const initialState = {
  detail_M: {},
  // movie_Trailer: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case MOVIES_DETAILS:
      //console.log(action.payload, 'result');
      return { ...state, detail_M: action.payload }

    // case MOVIES_TRAILER:
    //   // console.log(action.payload, "video")
    //   return { ...state, movie_Trailer: action.payload.results }


    default:
      return state;
  }
}