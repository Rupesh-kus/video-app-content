import { HOME_MOVIES, Home_TVSHOWS } from "../actions/Home"

const initialState = {
  homeMovies: [],
  homeTv: []
}
export default function (state = initialState, action) {
  switch (action.type) {
    case HOME_MOVIES:
      // console.log(action.payload, "reducer")
      return { ...state, homeMovies: action.payload.results }
    case Home_TVSHOWS:
      // console.log(action.payload, "reducerTv")
      return { ...state, homeTv: action.payload.results }
    default:
      return state;
  }
}