import { DISPLAY_MOVIES } from "../actions/displayMovies"
import { SEARCH_MOVIES } from "../actions/searchMovies"
const initialState = {
  display_M: [],
  searchQuery: "",
  searchResult: [],
}

export default function (state = initialState, action) {
  switch (action.type) {
    case DISPLAY_MOVIES:
      // console.log(action.payload, "reducer")
      return { ...state, display_M: action.payload.results }
    case SEARCH_MOVIES:
      // console.log(action.payload.results, 'search')
      return { ...state, searchResult: action.payload.results }
    default:
      return state;
  }
}