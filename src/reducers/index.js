import homePage from "./homePage"
import { combineReducers } from 'redux'
import displayMovies from "./displayMovies"
import displayTv from "./displayTv"
import detail_M from "./detailMovies"
import detail_Tv from "./detailTvShows"

export default combineReducers({
  homeVideos: homePage,
  movie: displayMovies,
  Tv_show: displayTv,
  movie_Details: detail_M,
  details: detail_Tv,
})