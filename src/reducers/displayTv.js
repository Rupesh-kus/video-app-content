import { display_TVSHOWS } from '../actions/actionDisplayTv'
import { SEARCH_TVSHOWS } from '../actions/searchTv'
const initialState = {
  display_Tv: [],
  searchQuery: "",
  searchResult: [],
}

export default function (state = initialState, action) {
  switch (action.type) {
    case display_TVSHOWS:
      // console.log(action.payload, "reducer")
      return { ...state, display_Tv: action.payload.results }
    case SEARCH_TVSHOWS:
      // console.log(action.payload.results, 'search')
      return { ...state, searchResult: action.payload.results }
    default:
      return state;
  }
}