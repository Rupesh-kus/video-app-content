import { TVSHOWS_DETAILS, TV_TRAILOR } from "../actions/tvShows_Details"

const initialState = {
  detail_Tv: {},
  trailer: []
}


export default function (state = initialState, action) {
  switch (action.type) {
    case TVSHOWS_DETAILS:
      // console.log(action.payload, 'result');
      return { ...state, detail_Tv: action.payload }
    case TV_TRAILOR:
      // console.log(action.payload, 'trailer');
      return { ...state, trailer: action.payload.results }
    default:
      return state;
  }
}
