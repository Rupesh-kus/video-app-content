import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from "./reducers";

const initialState = {};

const MiddleWare = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(...MiddleWare)
);

export default store;